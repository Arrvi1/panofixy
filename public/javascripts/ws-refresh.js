
function reloadOnEvents(game_id, events) {
    const protocol = location.protocol === 'https:' ? 'wss://' : 'ws://'
    const socket = new WebSocket(protocol + location.host + '/events/' + game_id)
    socket.addEventListener('message', event => {
        const data = JSON.parse(event.data)
        if (events.indexOf(data.type) !== -1) {
            location.reload()
        }
    })
}

