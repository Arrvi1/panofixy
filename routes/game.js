const express = require('express')
const router = express.Router()
const {Events} = require('../core/events')
const {Game, STATUS_STARTED, STATUS_NEW} = require('../models/game')
const {Player} = require('../models/player')
const {Turn} = require('../models/turn')
const {wrap} = require('./util')

const EVENT_PLAYER_JOINED = 'player_joined'
const EVENT_GAME_STARTED = 'game_started'

router.get('/create', wrap(async (req, res) => {
    const game_id = await Game.createNew()
    res.redirect(`/game/${game_id}/join`)
}))

router.get('/:game_id/join', wrap(async (req, res) => {
    if (req.cookies.player_id) {
        const player = await Player.get(req.cookies.player_id)
        if (player && player.game_id === parseInt(req.params.game_id)) {
            res.redirect(`/game/${player.game_id}/queue`)
            return
        }
    }
    res.render('join', {game_id: req.params.game_id})
}))

router.post('/:game_id/join', wrap(async (req, res) => {
    console.log(req.body)
    const player_id = await Player.create(req.params.game_id, req.body.name)
    Events.trigger(req.params.game_id, EVENT_PLAYER_JOINED)
    res.cookie('player_id', String(player_id), {path: '/'})
    res.redirect(`/game/${req.params.game_id}/queue`)
}))

router.get('/:game_id/queue', wrap(async (req, res) => {
    const game = await Game.get(req.params.game_id)
    if (game.status === STATUS_STARTED) {
        res.redirect(`/play/${game.game_id}`)
        return
    }

    const players = await Player.list(game.game_id)
    res.render('queue', {game, players, game_id: game.game_id, refresh_on: [EVENT_PLAYER_JOINED, EVENT_GAME_STARTED]})
}))

router.get('/:game_id/start', wrap(async (req, res) => {
    const gameId = req.params.game_id
    const game = await Game.get(gameId)
    if (game.status === STATUS_NEW) {
        await Game.start(gameId)
        const dealers = await Player.listNextDealers(gameId)
        await Turn.create(gameId, dealers[0].player_id)
        Events.trigger(req.params.game_id, EVENT_GAME_STARTED)
    }
    res.redirect(`/play/${gameId}`)
}))

module.exports = router
