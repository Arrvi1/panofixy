const express = require('express')
const router = express.Router()
const {Events} = require('../core/events')

router.ws('/:game_id', (ws, req) => {
    const id = Events.subscribe(req.params.game_id, event => {
        ws.send(JSON.stringify(event))
    })
    ws.on('close', () => {
        Events.unsubscribe(id)
    })
})

module.exports = router
