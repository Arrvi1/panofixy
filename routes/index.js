const express = require('express')
const router = express.Router()
const {wrap} = require('./util')
const {Game} = require('../models/game')

router.get('/', wrap(async (req, res) => {
    const games = await Game.listNew()
    res.render('index', {games, is_home: true})
}))

module.exports = router
