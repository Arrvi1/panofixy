const express = require('express')
const router = express.Router()

const {init_game} = require('../models/game')
const {init_player} = require('../models/player')
const {init_turn} = require('../models/turn')
const {init_definition} = require('../models/definition')
const {init_vote} = require('../models/vote')
const {init_score} = require('../models/score')
const {wrap} = require('./util')

router.post('/', wrap(async (req, res) => {
    await init_game()
    await init_player()
    await init_turn()
    await init_definition()
    await init_vote()
    await init_score()
    res.send('DONE\n')
}))

module.exports = router
