const express = require('express')
const router = express.Router()

const {Turn, PHASE_DEALER, PHASE_VOTE, PHASE_PLAYERS, PHASE_FINISHED} = require('../models/turn')
const {Game} = require('../models/game')
const {Player} = require('../models/player')
const {Definition} = require('../models/definition')
const {Vote} = require('../models/vote')
const {Score} = require('../models/score')
const {Events} = require('../core/events')
const {wrap} = require('./util')

const EVENT_WORD = 'word_set'
const EVENT_DEFINITION = 'definition_posted'
const EVENT_START_VOTING = 'voting_started'
const EVENT_VOTE = 'player_voted'
const EVENT_END_VOTING = 'voting_ended'
const EVENT_TURN = 'next_turn'
const EVENT_SKIP_ROUND = 'skip_deal'

router.get('/:game_id', wrap(async (req, res) => {
    const gameId = req.params.game_id
    const turn = await Turn.getCurrent(gameId)
    const isDealer = turn.dealer === parseInt(req.cookies.player_id)
    switch (turn.phase) {
        case PHASE_DEALER:
            if (isDealer) {
                res.redirect(`/play/${gameId}/deal`)
            } else {
                res.redirect(`/play/${gameId}/wait`)
            }
            break
        case PHASE_PLAYERS:
            if (await Definition.hasPlayerGuessed(req.cookies.player_id, turn.turn_id)) {
                if (isDealer) {
                    res.redirect(`/play/${gameId}/dealerWait`)
                } else {
                    res.redirect(`/play/${gameId}/waitForVote`)
                }
            } else {
                res.redirect(`/play/${gameId}/guess`)
            }
            break
        case PHASE_VOTE:
            if (isDealer) {
                res.redirect(`/play/${gameId}/dealerWait`)
            } else {
                res.redirect(`/play/${gameId}/vote`)
            }
            break
        case PHASE_FINISHED:
            res.redirect(`/play/${gameId}/score`)
            break
        default:
            console.log('Unknown phase', turn)
            throw new Error('Corrupted state')
    }
}))

router.get('/:game_id/deal', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_DEALER || turn.dealer !== parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        res.render('deal', {game_id: req.params.game_id})
    }
}))
router.post('/:game_id/deal', wrap(async (req, res) => {
    const gameId = req.params.game_id
    const turn = await Turn.getCurrent(gameId)
    if (turn.phase !== PHASE_DEALER || turn.dealer !== parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        await Turn.setWord(turn.turn_id, req.body.word)
        Events.trigger(gameId, EVENT_WORD)
        res.redirect(`/play/${gameId}/guess`)
    }
}))
router.get('/:game_id/skip', wrap(async (req, res) => {
    const gameId = req.params.game_id
    const turn = await Turn.getCurrent(gameId)
    if (turn.phase !== PHASE_DEALER || turn.dealer !== parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        const dealers = await Player.listNextDealers(gameId)
        await Turn.changeDealer(turn.turn_id, dealers[0].player_id)
        Events.trigger(gameId, EVENT_SKIP_ROUND)
        res.redirect(`/play/${gameId}`)
    }
}))

router.get('/:game_id/wait', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_DEALER
        || turn.dealer === parseInt(req.cookies.player_id)
        || await Definition.hasPlayerGuessed(req.cookies.player_id, turn.turn_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        res.render('wait', {turn, game_id: req.params.game_id, refresh_on: [EVENT_WORD, EVENT_SKIP_ROUND]})
    }
}))
router.get('/:game_id/dealerWait', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    const playersPhase = turn.phase === PHASE_PLAYERS
    const votePhase = turn.phase === PHASE_VOTE
    if (!playersPhase && !votePhase || turn.dealer !== parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        const players = await Player.listTurnStatus(turn.turn_id)
        res.render('dealerWait', {
            turn,
            players,
            game_id: req.params.game_id,
            canStartVote: playersPhase,
            canEndVote: votePhase,
            refresh_on: [EVENT_DEFINITION, EVENT_VOTE]
        })
    }
}))
router.get('/:game_id/startVoting', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_PLAYERS || turn.dealer !== parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        await Turn.startVoting(turn.turn_id)
        Events.trigger(req.params.game_id, EVENT_START_VOTING)
        res.redirect(`/play/${req.params.game_id}/dealerWait`)
    }
}))
router.get('/:game_id/endVoting', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_VOTE || turn.dealer !== parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        await Turn.endVoting(turn.turn_id)
        await Score.calculate(turn.turn_id)
        Events.trigger(req.params.game_id, EVENT_END_VOTING)
        res.redirect(`/play/${req.params.game_id}/score`)
    }
}))

router.get('/:game_id/guess', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_PLAYERS
        || await Definition.hasPlayerGuessed(req.cookies.player_id, turn.turn_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        res.render('guess', {turn, game_id: req.params.game_id})
    }
}))
router.post('/:game_id/guess', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_PLAYERS) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        await Definition.create(turn.turn_id, req.cookies.player_id, req.body.definition)
        Events.trigger(req.params.game_id, EVENT_DEFINITION)
        res.redirect(`/play/${req.params.game_id}`)
    }
}))
router.get('/:game_id/waitForVote', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_PLAYERS || turn.dealer === parseInt(req.cookies.player_id)
        || !await Definition.hasPlayerGuessed(req.cookies.player_id, turn.turn_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        const players = await Player.listTurnStatus(turn.turn_id)
        res.render('waitForVote', {
            players,
            game_id: req.params.game_id,
            refresh_on: [EVENT_DEFINITION, EVENT_START_VOTING]
        })
    }
}))

router.get('/:game_id/vote', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_VOTE || turn.dealer === parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        const definitions = await Definition.list(turn.turn_id)
        res.render('vote', {turn, definitions, game_id: req.params.game_id})
    }
}))
router.post('/:game_id/vote', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_VOTE || turn.dealer === parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        await Vote.create(req.body.def_id, req.cookies.player_id)
        Events.trigger(req.params.game_id, EVENT_VOTE)
        res.redirect(`/play/${req.params.game_id}/waitForFinish`)
    }
}))
router.get('/:game_id/waitForFinish', wrap(async (req, res) => {
    const turn = await Turn.getCurrent(req.params.game_id)
    if (turn.phase !== PHASE_VOTE || turn.dealer === parseInt(req.cookies.player_id)) {
        res.redirect(`/play/${req.params.game_id}`)
    } else {
        const definitions = await Definition.list(turn.turn_id)
        const players = await Player.listTurnStatus(turn.turn_id)
        res.render('waitForFinish', {
            turn,
            definitions,
            players,
            game_id: req.params.game_id,
            refresh_on: [EVENT_VOTE, EVENT_END_VOTING]
        })
    }
}))

router.get('/:game_id/score', wrap(async (req, res) => {
    const gameId = req.params.game_id
    const score = await Score.get(gameId)
    const players = await Player.list(gameId)
    const turns = await Turn.list(gameId)
    const turnId = turns.filter(t => t.phase === PHASE_FINISHED).map(t => t.turn_id).reduce((prev, turn) => turn)
    const definitions = await Definition.listWithVotes(turnId)
    res.render('score', {score, players, turns, definitions, game_id: gameId})
}))
router.get('/:game_id/next', wrap(async (req, res) => {
    const gameId = req.params.game_id
    const turn = await Turn.getCurrent(gameId)
    if (turn.phase === PHASE_FINISHED) {
        const dealers = await Player.listNextDealers(gameId)
        await Turn.create(gameId, dealers[0].player_id)
        Events.trigger(gameId, EVENT_TURN)
    }
    res.redirect(`/play/${gameId}`)
}))

module.exports = router
