const { Client } = require('pg')

const db = new Client({
    connectionString: process.env.DATABASE_URL,
    ssl: process.env.DATABASE_SSL ? process.env.DATABASE_SSL === 'true' : true,
})

db.connect()

module.exports.db = db
