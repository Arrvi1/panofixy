const subscribers = []
let lastId = 1

module.exports.Events = {
    subscribe (game_id, callback) {
        const subscriber = {
            id: lastId++,
            game_id: String(game_id),
            callback
        }
        subscribers.push(subscriber)
        return subscriber.id
    },
    unsubscribe (id) {
        const i = subscribers.findIndex(e => e.id === id)
        subscribers.splice(i, 1)
    },
    trigger (game_id, event, data = {}) {
        subscribers.filter(s => s.game_id === String(game_id))
            .forEach(s => s.callback({type: event, data}))
    }
}
