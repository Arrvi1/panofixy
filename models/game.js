const {db} = require('../core/db')

const STATUS_NEW = 'new'
const STATUS_STARTED = 'started'
const STATUS_ENDED = 'ended'
module.exports.STATUS_NEW = STATUS_NEW
module.exports.STATUS_STARTED = STATUS_STARTED
module.exports.STATUS_ENDED = STATUS_ENDED

module.exports.init_game = async () => {
    await db.query('DROP TABLE IF EXISTS Game CASCADE')
    await db.query(`CREATE TABLE Game
                    (
                        game_id serial PRIMARY KEY,
                        status  varchar(10) not null,
                        created timestamp default current_timestamp
                    )`)
    console.log('Initialized Game')
}

module.exports.Game = {
    createNew: async () => {
        const res = await db.query('INSERT INTO Game (status) VALUES ($1) RETURNING game_id', [STATUS_NEW])
        const gameId = res.rows[0].game_id
        console.log('Created game', gameId)
        return gameId
    },
    get: async (game_id) => {
        const res = await db.query('SELECT * FROM Game WHERE game_id = $1', [game_id])
        return res.rows[0]
    },
    listNew: async () => {
        const res = await db.query(`SELECT g.game_id, count(p.player_id) as players
                                    FROM Game g
                                             left JOIN player p on g.game_id = p.game_id
                                    WHERE g.status = $1
                                    group by g.game_id`,
            [STATUS_NEW])
        return res.rows
    },
    start: async (game_id) => {
        await db.query('UPDATE Game SET status = $1 WHERE game_id = $2', [STATUS_STARTED, game_id])
        console.log('Started game', game_id)
    },
    end: async (game_id) => {
        await db.query('UPDATE Game SET status = $1 WHERE game_id = $2', [STATUS_ENDED, game_id])
        console.log('Ended game', game_id)
    }
}
