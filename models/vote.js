const {db} = require('../core/db')

module.exports.init_vote = async () => {
    await db.query('DROP TABLE IF EXISTS Vote CASCADE')
    await db.query(`CREATE TABLE Vote
                    (
                        vote_id   serial primary key,
                        def_id    char(32) not null,
                        turn_id   int      not null,
                        player_id int      not null,
                        constraint vt_tr_pl_uq unique (turn_id, player_id),
                        constraint vt_df_fk foreign key (def_id) references definition (def_id),
                        constraint vt_tr_fk foreign key (turn_id) references turn (turn_id),
                        constraint vt_pl_fk foreign key (player_id) references player (player_id)
                    )`)
    console.log('Initialized Vote')
}

module.exports.Vote = {
    create: async (def_id, player_id) => {
        const res = await db.query(`
            INSERT INTO Vote (def_id, player_id, turn_id)
            SELECT $1 as def_id, $2 as player_id, turn_id
            FROM definition
            WHERE def_id = $1
            RETURNING vote_id, turn_id
        `, [def_id, player_id])
        const voteId = res.rows[0].vote_id
        console.log('Created vote', voteId, 'turn', res.rows[0].turn_id, 'player', player_id, 'definition', def_id)
        return voteId
    },
    listForDefinitions: async (turn_id) => {
        const res = await db.query(`
            SELECT d.def_id, d.content, d.player_id as author, v.player_id as voter
            FROM definition d
                     LEFT JOIN vote v on d.def_id = v.def_id
            WHERE d.turn_id = $1
        `, [turn_id])
        return res.rows
    }
}
