const {db} = require('../core/db')

const PHASE_DEALER = 'dealer'
const PHASE_PLAYERS = 'players'
const PHASE_VOTE = 'vote'
const PHASE_FINISHED = 'finished'

module.exports = {
    PHASE_DEALER,
    PHASE_PLAYERS,
    PHASE_VOTE,
    PHASE_FINISHED
}

module.exports.init_turn = async () => {
    await db.query('DROP TABLE IF EXISTS Turn CASCADE')
    await db.query(`CREATE TABLE Turn
                    (
                        turn_id serial primary key,
                        game_id int          not null,
                        dealer  int          not null,
                        phase   varchar(10)  not null,
                        word    varchar(100) null,
                        constraint tr_gm_fk foreign key (game_id) references game (game_id) on delete cascade,
                        constraint tr_pl_fk foreign key (dealer) references player (player_id) on delete restrict
                    )`)
    console.log('Initialized Turn')
}

module.exports.Turn = {
    create: async (game_id, dealer) => {
        const res = await db.query('INSERT INTO Turn (game_id, dealer, phase) values ($1, $2, $3) returning turn_id', [game_id, dealer, PHASE_DEALER])
        const turnId = res.rows[0].turn_id
        console.log("Created turn", turnId, 'game', game_id, 'dealer', dealer)
        return turnId
    },
    get: async (turn_id) => {
        const res = await db.query('SELECT * FROM Turn WHERE turn_id = $1', [turn_id])
        return res.rows[0]
    },
    list: async (game_id) => {
        const res = await db.query('SELECT * FROM Turn WHERE game_id = $1 order by turn_id', [game_id])
        return res.rows
    },
    getCurrent: async (game_id) => {
        const res = await db.query(`SELECT t.*, p.name as dealer_name
                                    FROM turn t
                                             JOIN player p
                                                  on p.player_id = t.dealer
                                    WHERE t.game_id = $1
                                    ORDER BY t.turn_id DESC
                                    LIMIT 1`, [game_id])
        return res.rows[0]
    },
    setWord: async (turn_id, word) => {
        await db.query(`UPDATE turn SET word = $1, phase = $2 WHERE turn_id = $3`, [word, PHASE_PLAYERS, turn_id])
        console.log ('Set word', word, 'turn', turn_id, '(changed state to players)')
    },
    changeDealer: async (turn_id, dealer) => {
        await db.query('UPDATE turn SET dealer = $1 WHERE turn_id = $2', [dealer, turn_id])
        console.log('Changed dealer', dealer, 'turn', turn_id)
    },
    startVoting: async (turn_id) => {
        await db.query(`UPDATE turn SET phase = $1 WHERE turn_id = $2`, [PHASE_VOTE, turn_id])
        console.log('Started voting for turn', turn_id)
    },
    endVoting: async (turn_id) => {
        await db.query(`UPDATE turn SET phase = $1 WHERE turn_id = $2`, [PHASE_FINISHED, turn_id])
        console.log('Finished turn', turn_id)
    },
}
