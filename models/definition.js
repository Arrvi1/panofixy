const {db} = require('../core/db')
const md5 = require('md5')

module.exports.init_definition = async () => {
    await db.query('DROP TABLE IF EXISTS Definition CASCADE')
    await db.query(`CREATE TABLE Definition
                    (
                        def_id    char(32) primary key,
                        player_id int              not null,
                        turn_id   int              not null,
                        content   text             not null,
                        position  double precision not null default random(),
                        constraint df_pl_tr_uq unique (player_id, turn_id),
                        constraint df_pl_fk foreign key (player_id) references player (player_id) on delete cascade,
                        constraint df_tr_fk foreign key (turn_id) references turn (turn_id) on delete cascade
                    )`)
    console.log('Initialized Definition')
}

module.exports.Definition = {
    create: async (turn_id, player_id, content) => {
        const res = await db.query(
            'INSERT INTO definition (def_id, player_id, turn_id, content) VALUES ($1, $2, $3, $4) returning def_id',
            [md5(`${player_id}${turn_id}${content}`), player_id, turn_id, content])
        const defId = res.rows[0].def_id
        console.log('Created definition', defId, 'turn', turn_id, 'player', player_id)
        return defId
    },
    list: async (turn_id) => {
        const res = await db.query('SELECT * FROM definition WHERE turn_id = $1 ORDER BY position', [turn_id])
        return res.rows
    },
    listWithVotes: async (turn_id) => {
        const res = await db.query(`
            SELECT p.player_id, p.name, d.content, count(v.vote_id) as votes, (p.player_id = t.dealer) as real
            FROM player p
                     LEFT JOIN definition d on p.player_id = d.player_id
                     LEFT JOIN vote v on d.def_id = v.def_id
                    JOIN turn t on t.game_id = p.game_id and t.turn_id = $1
            WHERE d.turn_id = $1
               or d.turn_id is null
            GROUP BY p.player_id, p.name, d.content, t.dealer
            ORDER BY (p.player_id = t.dealer) desc
        `, [turn_id])
        return res.rows
    },
    hasPlayerGuessed: async (player_id, turn_id) => {
        const res = await db.query(`SELECT def_id
                                    FROM definition
                                    where player_id = $1
                                      and turn_id = $2`, [player_id, turn_id])
        return !!res.rows.length
    }
}
