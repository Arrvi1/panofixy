const {db} = require('../core/db')
const {Turn} = require('./turn')
const {Vote} = require('./vote')
const {Player} = require('./player')

const DEALER_NOT_GUESSED_SCORE = 2
const DEALER_GUESSED_SCORE = 0
const PLAYER_GUESSED_SCORE = 2
const PLAYER_VOTE_SCORE = 1

module.exports.init_score = async () => {
    await db.query('DROP TABLE IF EXISTS Score CASCADE')
    await db.query(`CREATE TABLE Score
                    (
                        score_id  serial primary key,
                        player_id int not null,
                        turn_id   int not null,
                        score     int not null,
                        constraint sc_pl_tr_uq unique (player_id, turn_id),
                        constraint sc_pl_fk foreign key (player_id) references player (player_id),
                        constraint sc_tr_fk foreign key (turn_id) references turn (turn_id)
                    )`)
    console.log('Initialized Score')
}

module.exports.Score = {
    get: async (game_id) => {
        const res = await db.query('SELECT s.* FROM score s JOIN turn t ON s.turn_id = t.turn_id WHERE t.game_id = $1', [game_id])
        return res.rows.reduce((obj, row)=>{
            obj[row.player_id] = obj[row.player_id] || {}
            obj[row.player_id][row.turn_id] = row.score
            return obj
        }, {})
    },
    calculate: async (turn_id) => {
        try {
            await db.query('BEGIN')
            const turn = await Turn.get(turn_id)
            const votes = (await Vote.listForDefinitions(turn_id)).reduce((acc, def) => {
                acc[def.author] = acc[def.author] || []
                if (def.voter) {
                    acc[def.author].push(def.voter)
                }
                return acc
            }, {})
            const players = await Player.list(turn.game_id)
            const score = players
                .map(player => player.player_id)
                .map(player_id => {
                    if (player_id === turn.dealer) {
                        return {
                            player: player_id,
                            score: !votes[player_id].length ? DEALER_NOT_GUESSED_SCORE : DEALER_GUESSED_SCORE
                        }
                    } else {
                        const voteScore = votes[player_id].length * PLAYER_VOTE_SCORE
                        const guessScore = (votes[turn.dealer].indexOf(player_id) !== -1) ? PLAYER_GUESSED_SCORE : 0
                        return {
                            player: player_id,
                            score: voteScore + guessScore
                        }
                    }
                })
            const rowsNum = score.length
            const data = score.reduce((arr, row) => {
                arr.push(turn.turn_id)
                arr.push(row.player)
                arr.push(row.score)
                return arr
            }, [])
            const placeholders = []
            for (let i = 1; i <= rowsNum * 3; i += 3) {
                placeholders.push(`($${i}, $${i+1}, $${i+2})`)
            }
            await db.query(`INSERT INTO Score (turn_id, player_id, score) VALUES ${placeholders.join(',')}`, data)
            await db.query('COMMIT')
            console.log('Calculated score for turn', turn.turn_id, 'score', score)
        } catch (e) {
            await db.query('ROLLBACK')
            throw e
        }
    }
}

