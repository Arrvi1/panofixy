const {db} = require('../core/db')

module.exports.init_player = async () => {
    await db.query('DROP TABLE IF EXISTS Player CASCADE')
    await db.query(`CREATE TABLE Player
                    (
                        player_id serial primary key,
                        name      varchar(100) not null,
                        game_id   int          not null,
                        CONSTRAINT pl_nm_gm_uq UNIQUE (name, game_id),
                        CONSTRAINT pl_gm_fk FOREIGN KEY (game_id) REFERENCES Game (game_id) MATCH SIMPLE ON DELETE CASCADE
                    )`)
    console.log('Initialized Player')
}

module.exports.Player = {
    create: async (game_id, name) => {
        const res = await db.query(
            'INSERT INTO Player (name, game_id) VALUES ($1, $2) RETURNING player_id',
            [name, game_id])
        const playerId = res.rows[0].player_id
        console.log('Created player', playerId, 'game', game_id)
        return playerId
    },
    get: async (player_id) => {
        const res = await db.query('SELECT * FROM player where player_id = $1', [player_id])
        return res.rows[0]
    },
    list: async (game_id) => {
        const res = await db.query('SELECT * FROM Player WHERE game_id = $1', [game_id])
        return res.rows
    },
    listTurnStatus: async (turn_id) => {
        const res = await db.query(`
            SELECT p.*, (d.def_id is not null) as guessed, (v.vote_id is not null) as voted, d.content
            FROM Player p
                     LEFT JOIN definition d
                               ON p.player_id = d.player_id and d.turn_id = $1
                     LEFT JOIN vote v
                               on p.player_id = v.player_id and v.turn_id = $1
                     JOIN turn t
                          on p.game_id = t.game_id and t.turn_id = $1
        `, [turn_id])
        return res.rows
    },
    listNextDealers: async (game_id) => {
        const res = await db.query(`SELECT *
                                    FROM Player p
                                    WHERE p.game_id = $1
                                    ORDER BY (
                                                 SELECT p.player_id > t.dealer
                                                 FROM Turn t
                                                 ORDER BY t.turn_id
                                                     DESC
                                                 LIMIT 1
                                             ) desc,
                                             p.player_id`, [game_id])
        return res.rows
    }
}
