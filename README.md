# Panofixy
Game client/server. Kinda private project

## Requirements

* Node (works on v12.14, did not check earlier)
* Yarn (https://yarnpkg.com/)
* docker (https://www.docker.com/)
* docker-compose (https://docs.docker.com/compose/)
* (optional) PostgreSQL server instead of docker

## Running local

### Database
```
cd docker/panofixy
docker-compose up -d
```
or create Postgres base on `5432` port with `postgres` db, `postgres` user and `local` password

### Server
```
yarn install
yarn develop
```
Application should be visible at localhost:3000

### Database init
```
curl -X POST http://localhost:3000/init
```
